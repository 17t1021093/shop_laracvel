document.addEventListener("DOMContentLoaded",function(){
    var province=document.getElementById('province');
    window.onload=function(){
        $.ajax({
            url: 'https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/province',
            headers: {
                'token' : '07789055-b334-11eb-8080-c61f6a6501ca',
                'Content-Type' : 'application/json'            
            },
            method: 'GET',
            dataType: 'json',
            success: function(response){
                console.log('success: ');
                console.log(response.data);
                var str="<option selected>Tỉnh thành</option>";
                for(var i=0;i<response.data.length;i++){
                    console.log(response.data[i].ProvinceName);
                    str=str+"<option class='provinceId' data-province='"+response.data[i].ProvinceID+"' >" +"</option>"
                }
                province.innerHTML=str;
            }
        });
    }
},false);

function changeFunc(){
    var selectBox = document.getElementById("province");
    var selectedValue = selectBox.options[selectBox.selectedIndex].getAttribute('data-province');
    var district = document.getElementById('district');
    $.ajax({
        url: 'https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/district',
        headers: {
            'token' : '07789055-b334-11eb-8080-c61f6a6501ca',
            'Content-Type' : 'application/json'
        },
        method: 'GET',
        dataType: 'json',
        success: function(response){
            var str="<option selected>Quận huyện</option>";
            for(var i=0;i<response.data.length;i++){
                if(response.data[i].ProvinceID==selectedValue)
                str=str+"<option class='districtId' data-district='"+response.data[i].DistrictID+"' >" +"</option>"
            }
            district.innerHTML=str;
        }
    });
};
function changeFuncDistrict(){
    var selectBox = document.getElementById("district");
    var selectedValue = selectBox.options[selectBox.selectedIndex].getAttribute('data-district');
    var ward = document.getElementById('ward');
    $.ajax({
        url: 'https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/ward?district_id='+selectedValue,
        headers: {
            'token' : '07789055-b334-11eb-8080-c61f6a6501ca',
            'Content-Type' : 'application/json'
        },
        method: 'GET',
        dataType: 'json',
        success: function(response){
            var str="<option selected>Phường xã</option>";
            for(var i=0;i<response.data.length;i++){
                str=str+"<option class='wardId' data-ward='"+response.data[i].WardCode+"' >" +"</option>"
            }
            ward.innerHTML=str;
        }
    });
};